﻿using System;
using System.Collections.Generic;
using System.Linq;

public interface IVector
{
    int this[int i] { get; set; }
    void AddElement(int element);
    void RemoveElement(int index);
    int GetLenght();
    int GetCountValuesEqualsTwo();
    bool IsOrthogonality(IVector vector);
    IVector Intersection(IVector vector);
    

}

public class Vector:IVector
{
    private List<int> _values;

    public int this[int i]
    {
        get { return _values[i]; }
        set
        {
            if (CheckValue(value))
            {
                _values[i] = value;
            }
            else
            {
                throw new ArgumentException("The variable value must be 0, 1, 2");
            }
        }
    }
    //add element vector
    public void AddElement(int element)
    {
        if (CheckValue(element))
        {
            _values.Add(element);
        }
        else
        {
            throw new ArgumentException("The variable value must be 0, 1, 2");
        }
    }
    //remove element vector
    public void RemoveElement(int index)
    {
        _values.Remove(index);
    }

    public int GetLenght()
    {
        return _values.Count;
    }
    // check correct values 0,1,2
    private bool CheckValue(int value)
    {
        if (value < 0 || value > 2)
            return false;
        return true;
    }
    // check correct values 0,1,2
    private bool CheckRangeValues(int[] valuesOfVector)
    {
        return valuesOfVector.All(CheckValue);
    }

    public Vector()
    {
        _values = new List<int>();
    }

    public Vector(int[] valuesOfVector)
    {
        if (CheckRangeValues(valuesOfVector))
        {
            _values = new List<int>();
            _values.AddRange(valuesOfVector);
        }
        else
        {
            throw new ArgumentException("The variable value must be 0, 1, 2");
        }
    }
    // return IsOrthogonality
    public bool IsOrthogonality(IVector vector)
    {
        if (_values.Count != vector.GetLenght())
        {
            throw new ArgumentException("Vector lengths must be equal.");
        }
        int scalarProduct = 0;
        for (int i = 0; i < _values.Count; i++)
        {
            scalarProduct += _values[i]*vector[i];
        }
        if (scalarProduct == 0)
        {
            return true;
        }
        return false;
    }
    // return vector Intersection
    public IVector Intersection(IVector vector)
    {
        Vector intersectionVector = new Vector();
        if (IsOrthogonality(vector))
        {
            throw new ArgumentException("The input vector is orthogonal");
        }
        for (int i = 0; i < _values.Count; i++)
        {
            if ((_values[i] == 1 && vector[i] == 1) ||
                (_values[i] == 1 && vector[i] == 2) ||
                (_values[i] == 2 && vector[i] == 1))
            {
                intersectionVector.AddElement(1);
            }
            else
            {
                if (_values[i] == 2 && vector[i] == 2)
                {
                    intersectionVector.AddElement(2);
                }
                else
                {
                    intersectionVector.AddElement(0);
                }
            }
        }
        return intersectionVector;
    }
    // return count elements of vector with value 2
    public int GetCountValuesEqualsTwo()
    {
        return _values.Count(value => value == 2);
    }
}


class Program
{
    private static void Main()
    {
        Console.WriteLine("___Task3___");
        Console.WriteLine();

        Console.WriteLine("**Create vector**");
        IVector vector1 = new Vector(new[] {1, 1, 0});
        IVector vector2 = new Vector(new[] {0, 2, 2});

        Console.WriteLine("**Vecotors IsOrthogonality**");
        Console.WriteLine("Result :{0}",vector1.IsOrthogonality(vector2));
        Console.WriteLine();

        Console.WriteLine("**Vecotors Intersection**");
        IVector vector3 = vector1.Intersection(vector2);
        for (int i = 0; i < vector3.GetLenght(); i++)
        {
            Console.WriteLine("Element {0}: {1} ",i,vector3[i]);
        }
        Console.WriteLine();

        Console.WriteLine("**Get Count Values Equals Two**");
        Console.WriteLine("Result: {0}", vector2.GetCountValuesEqualsTwo());
    }
}

